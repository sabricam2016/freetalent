﻿using System;
using System.Threading.Tasks;
using Freetalent.Common.Domain;
using Freetalent.Common.User;
using Freetalent.Entityframework.TypeConfigurations;
using Microsoft.EntityFrameworkCore;

namespace Freetalent.Entityframework
{
    public class AppDbContext : DbContext
    {
        private readonly ICurrentUser _currentUser;
        public AppDbContext(DbContextOptions<AppDbContext> options, IUserProvider userProvider) : base(options)
        {
            _currentUser = userProvider.GetCurrentUser();
        }

        public AppDbContext()
        {
        }
       
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.HasDefaultSchema("Freetalent");
            
            modelBuilder.ApplyConfiguration(new AccountTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ProjectTypeConfiguration());
            
        }
        
        public async Task SaveChangesAsync()
        {
           await OnBeforeSaveChanges();
           await base.SaveChangesAsync();
        }
        
        private Task OnBeforeSaveChanges()
        {
            var entries = this.ChangeTracker.Entries();
            foreach (var ent in entries)
            {
                if (ent.Entity is IAggregateRoot entity)
                {
                    entity.CreatedDate = DateTime.Now;
                    entity.CreatedBy = _currentUser?.TenantId ?? Guid.Empty;
                }
            }
            
            return Task.CompletedTask;
        }
    }
}