﻿using System;
using System.Threading.Tasks;
using System.Transactions;
using Freetalent.Common.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Freetalent.Entityframework
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _dbContext;
        private readonly ILogger<UnitOfWork> _logger;

        public UnitOfWork(AppDbContext dbContext, ILogger<UnitOfWork> logger)
        {
            _dbContext = dbContext;
            _logger = logger;
        }

        public async Task RunInDbTransaction(Func<Task> action)
        {
            try
            {
                var strategy = _dbContext.Database.CreateExecutionStrategy();
                await strategy.ExecuteAsync(async () =>
                {
                    using (var transaction = await _dbContext.Database.BeginTransactionAsync())
                    {
                        await action();
                        transaction.Commit();
                        // aynı transaction içinde yapılan farklı işler bu transaction içinde yapılarak savechanges yapılabilir
                    }
                });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Database transaction could not commit {ex.Message}");
            }
        }

        public async Task RunInTransactionScope(Func<Task> action)
        {
            try
            {
                var strategy = _dbContext.Database.CreateExecutionStrategy();
                await strategy.ExecuteAsync(async () =>
                {
                    using (var transactionScope = new TransactionScope())
                    {
                        await action();
                        transactionScope.Complete();
                        // multiple dbContex, bu scope içerisinde saveChanges yaparak consistency sağlanıyor
                    }
                });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Database transaction could not commit {ex.Message}");
            }
        }

        public async Task<T> RunInTransactionScope<T>(Func<Task<T>> action)
        {
            var strategy = _dbContext.Database.CreateExecutionStrategy();
            return await strategy.ExecuteAsync(async () =>
            {
                using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    var result = await action();
                    transactionScope.Complete();
                    return result;
                    // multiple dbContex, bu scope içerisinde saveChanges yaparak consistency sağlanıyor
                }
            });
        }

        public async Task SaveChangesAsync()
        {
            try
            {
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Could not save changes on db {ex.Message}");
            }
        }

        public void Dispose()
        {
            _dbContext?.Dispose();
        }
    }
}