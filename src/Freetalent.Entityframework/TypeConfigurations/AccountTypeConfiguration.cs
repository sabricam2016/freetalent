﻿using System;
using Freetalent.Domain.Accounts;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Freetalent.Entityframework.TypeConfigurations
{
    public class AccountTypeConfiguration : IEntityTypeConfiguration<Account>
    {
        public void Configure(EntityTypeBuilder<Account> builder)
        {
            builder.ToTable("accounts");
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Username).HasColumnName("username")
                .HasMaxLength(AccountConsts.UsernameMaxLength);
            builder.Property(x => x.Firstname).HasColumnName("firstname")
                .HasMaxLength(AccountConsts.FirstnameMaxLength);
            builder.Property(x => x.Lastname).HasColumnName("lastname")
                .HasMaxLength(AccountConsts.LastnameMaxLength);
            builder.Property(x => x.AccountType).HasColumnName("account_type").IsRequired();

            builder.OwnsMany(x => x.Addresses, b =>
            {
                b.ToTable("account_addresses");
                b.Property(x => x.PhoneCode).HasColumnName("phone_code")
                    .HasMaxLength(AddressConsts.MaxLengthPhoneCode);
                b.Property(x => x.PhoneNumber).HasColumnName("phone_number")
                    .HasMaxLength(AddressConsts.MaxLengthPhoneNumber);
                b.Property(x => x.Email).HasColumnName("email").HasMaxLength(AddressConsts.MaxLengthEmail);

                b.Property<Guid>("AccountId");
                b.WithOwner().HasForeignKey("AccountId");
            });
        }
    }
}