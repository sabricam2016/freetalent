﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Freetalent.Common.Domain;
using Freetalent.Common.Repository;
using Microsoft.EntityFrameworkCore;

namespace Freetalent.Entityframework.Repositories
{
    public class Repository<TEntity, TId> : IRepository<TEntity, TId>
        where TEntity : AggregateRoot<TId>, new() where TId : IEquatable<TId>
    {
        protected readonly DbSet<TEntity> _dbTable;

        public Repository(AppDbContext dbContext)
        {
            _dbTable = dbContext.Set<TEntity>();
        }

        public void Create(TEntity entity)
        {
           _dbTable.Add(entity);
        }

        public void Update(TEntity entity)
        {
            _dbTable.Update(entity);
        }

        public async Task<TEntity> GetById(TId id)
        {
            return await _dbTable.FindAsync(id);
        }

        public async Task<IList<TEntity>> GetList()
        {
            return await _dbTable.ToListAsync();
        }

        public Task<TEntity> GetById(TId id, params Expression<Func<TEntity, object>>[] includes)
        {
            var query = _dbTable.AsQueryable();

            foreach (var include in includes)
                query = query.Include(include).AsQueryable();

            return query.FirstOrDefaultAsync(x => x.Id.Equals(id));
        }

        public async Task<IList<TEntity>> GetList(params Expression<Func<TEntity, object>>[] includes)
        {
            var query = _dbTable.AsQueryable();
            foreach (var include in includes)
                query = query.Include(include).AsQueryable();

            return await query.ToListAsync();
        }

        public IQueryable<TEntity> GetQueryAsNoTracking()
        {
            return _dbTable.AsNoTracking();
        }

        public IQueryable<TEntity> GetQuery()
        {
            return _dbTable.AsQueryable();
        }
    }
}