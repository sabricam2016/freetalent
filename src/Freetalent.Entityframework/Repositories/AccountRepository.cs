﻿using System;
using Freetalent.Domain.Accounts;

namespace Freetalent.Entityframework.Repositories
{
    public class AccountRepository : Repository<Account, Guid>, IAccountRepository
    {
        public AccountRepository(AppDbContext dbContext) : base(dbContext)
        {
        }
    }
}