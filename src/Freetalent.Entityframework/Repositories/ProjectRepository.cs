﻿using System;
using System.Threading.Tasks;
using Freetalent.Domain.Projects;

namespace Freetalent.Entityframework.Repositories
{
    public class ProjectRepository : Repository<Project, Guid>, IProjectRepository
    {
        public ProjectRepository(AppDbContext dbContext) : base(dbContext)
        {
            
        }

        public Task Add(Project entity)
        {
            _dbTable.Add(entity);
            return Task.CompletedTask;
        }
    }
}