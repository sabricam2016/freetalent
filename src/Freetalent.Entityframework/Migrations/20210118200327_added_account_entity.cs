﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Freetalent.Entityframework.Migrations
{
    public partial class added_account_entity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "Freetalent");

            migrationBuilder.CreateTable(
                name: "accounts",
                schema: "Freetalent",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    username = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    firstname = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    lastname = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    account_type = table.Column<int>(type: "integer", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    CreatedBy = table.Column<Guid>(type: "uuid", nullable: false),
                    UpdatedBy = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_accounts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Address",
                schema: "Freetalent",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    AccountId = table.Column<Guid>(type: "uuid", nullable: false),
                    phone_code = table.Column<string>(type: "character varying(5)", maxLength: 5, nullable: true),
                    phone_number = table.Column<string>(type: "character varying(11)", maxLength: 11, nullable: true),
                    email = table.Column<string>(type: "character varying(30)", maxLength: 30, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Address", x => new { x.AccountId, x.Id });
                    table.ForeignKey(
                        name: "FK_Address_accounts_AccountId",
                        column: x => x.AccountId,
                        principalSchema: "Freetalent",
                        principalTable: "accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Address",
                schema: "Freetalent");

            migrationBuilder.DropTable(
                name: "accounts",
                schema: "Freetalent");
        }
    }
}
