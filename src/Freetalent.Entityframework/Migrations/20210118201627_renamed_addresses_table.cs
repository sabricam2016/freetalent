﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Freetalent.Entityframework.Migrations
{
    public partial class renamed_addresses_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Address_accounts_AccountId",
                schema: "Freetalent",
                table: "Address");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Address",
                schema: "Freetalent",
                table: "Address");

            migrationBuilder.RenameTable(
                name: "Address",
                schema: "Freetalent",
                newName: "addresses",
                newSchema: "Freetalent");

            migrationBuilder.AddPrimaryKey(
                name: "PK_addresses",
                schema: "Freetalent",
                table: "addresses",
                columns: new[] { "AccountId", "Id" });

            migrationBuilder.AddForeignKey(
                name: "FK_addresses_accounts_AccountId",
                schema: "Freetalent",
                table: "addresses",
                column: "AccountId",
                principalSchema: "Freetalent",
                principalTable: "accounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_addresses_accounts_AccountId",
                schema: "Freetalent",
                table: "addresses");

            migrationBuilder.DropPrimaryKey(
                name: "PK_addresses",
                schema: "Freetalent",
                table: "addresses");

            migrationBuilder.RenameTable(
                name: "addresses",
                schema: "Freetalent",
                newName: "Address",
                newSchema: "Freetalent");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Address",
                schema: "Freetalent",
                table: "Address",
                columns: new[] { "AccountId", "Id" });

            migrationBuilder.AddForeignKey(
                name: "FK_Address_accounts_AccountId",
                schema: "Freetalent",
                table: "Address",
                column: "AccountId",
                principalSchema: "Freetalent",
                principalTable: "accounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
