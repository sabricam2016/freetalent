﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Freetalent.Entityframework.Migrations
{
    public partial class renamed_addresses_as_account_adresses : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_addresses_accounts_AccountId",
                schema: "Freetalent",
                table: "addresses");

            migrationBuilder.DropPrimaryKey(
                name: "PK_addresses",
                schema: "Freetalent",
                table: "addresses");

            migrationBuilder.RenameTable(
                name: "addresses",
                schema: "Freetalent",
                newName: "account_addresses",
                newSchema: "Freetalent");

            migrationBuilder.AddPrimaryKey(
                name: "PK_account_addresses",
                schema: "Freetalent",
                table: "account_addresses",
                columns: new[] { "AccountId", "Id" });

            migrationBuilder.AddForeignKey(
                name: "FK_account_addresses_accounts_AccountId",
                schema: "Freetalent",
                table: "account_addresses",
                column: "AccountId",
                principalSchema: "Freetalent",
                principalTable: "accounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_account_addresses_accounts_AccountId",
                schema: "Freetalent",
                table: "account_addresses");

            migrationBuilder.DropPrimaryKey(
                name: "PK_account_addresses",
                schema: "Freetalent",
                table: "account_addresses");

            migrationBuilder.RenameTable(
                name: "account_addresses",
                schema: "Freetalent",
                newName: "addresses",
                newSchema: "Freetalent");

            migrationBuilder.AddPrimaryKey(
                name: "PK_addresses",
                schema: "Freetalent",
                table: "addresses",
                columns: new[] { "AccountId", "Id" });

            migrationBuilder.AddForeignKey(
                name: "FK_addresses_accounts_AccountId",
                schema: "Freetalent",
                table: "addresses",
                column: "AccountId",
                principalSchema: "Freetalent",
                principalTable: "accounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
