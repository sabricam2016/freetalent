﻿using System;

namespace Freetalent.SampleTwo
{
    public class SampleTwoClass
    {
        public string CombineName(string firstName, string lastName)
        {
            return firstName + " " + lastName;
        }
        
        public string SecondCombineName(string firstName, string lastName)
        {
            return firstName + " " + lastName;
        }
        
        public string ThirdCombineName(string firstName, string lastName)
        {
            return firstName + " " + lastName;
        }
        
        public string FourthCombineName(string firstName, string lastName)
        {
            return firstName + " " + lastName;
        }
        
        public string FifthCombineName(string firstName, string lastName)
        {
            return firstName + " " + lastName;
        }
        
        public string SixthCombineName(string firstName, string lastName)
        {
            return firstName + " " + lastName;
        }
        
        public string SeventhCombineName(string firstName, string lastName)
        {
            return firstName + " " + lastName;
        }
    }
}