using System;
using Freetalent.IdentityServer.Api.Configurations;
using Freetalent.IdentityServer.Api.IdentityService;
using Freetalent.Master.Domain.Users;
using Freetalent.Master.Entityframework;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Freetalent.IdentityServer.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {  
            services.AddControllers();
            
            services.AddMySwagger();
            
            services.AddDbContext<MasterDbContext>(options =>
                options.UseNpgsql(Configuration.GetConnectionString("Default")));

            services.AddIdentity<User, IdentityRole<Guid>>()
                .AddEntityFrameworkStores<MasterDbContext>()
                .AddDefaultTokenProviders();
            
            services.AddMyIdentityServer();
            IdentityServerSettings.Authority = Configuration.GetSection("Authority").Value;
            services.AddHttpClient<IIdentityService, IdentityService.IdentityService>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMySwagger();
            app.UseHttpsRedirection();
            
            app.UseIdentityServer();
            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}