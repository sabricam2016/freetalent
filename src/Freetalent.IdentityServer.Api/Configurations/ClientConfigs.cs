﻿using System.Collections.Generic;
using IdentityServer4.Models;

namespace Freetalent.IdentityServer.Api.Configurations
{
    public static class ClientConfigs
    {
        public static List<Client> GetClients()
        {
            return new List<Client>
            {
                new Client
                {
                    ClientId = "ft.api.client",
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPasswordAndClientCredentials,
                    AllowOfflineAccess = true,
                    AccessTokenLifetime = 36000,
                    IdentityTokenLifetime = 36000,
                    RefreshTokenExpiration = TokenExpiration.Sliding,
                    RefreshTokenUsage = TokenUsage.OneTimeOnly,
                    UpdateAccessTokenClaimsOnRefresh = true,
                    ClientSecrets = {new Secret("ft.api.secret".Sha256())},
                    AllowedScopes = {"ft.api.scope"}
                }
            };
        }


        public static List<ApiScope> GetScopes()
        {
            return new List<ApiScope>
            {
                new ApiScope("ft.api.scope")
            };
        }

        public static List<ApiResource> GetResources()
        {
            return new List<ApiResource>()
            {
                new ApiResource("ft.api.source") { Scopes = { "ft.api.scope" } }
            };
        }
    }
}