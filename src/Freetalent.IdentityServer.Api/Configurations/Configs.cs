﻿using Freetalent.Master.Domain.Users;
using Microsoft.Extensions.DependencyInjection;

namespace Freetalent.IdentityServer.Api.Configurations
{
    public static class Configs
    {
        public static void AddMyIdentityServer(this IServiceCollection services)
        {
            services
                .AddIdentityServer()
                .AddAspNetIdentity<User>()
                .AddDeveloperSigningCredential()
                .AddInMemoryClients(ClientConfigs.GetClients())
                .AddInMemoryApiResources(ClientConfigs.GetResources())
                .AddInMemoryApiScopes(ClientConfigs.GetScopes())
                .AddProfileService<MyProfileService>();
        }
    }
}