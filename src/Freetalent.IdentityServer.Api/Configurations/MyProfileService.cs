﻿using System.Linq;
using System.Threading.Tasks;
using Freetalent.Master.Domain.Users;
using IdentityServer4.Extensions;
using IdentityServer4.Models;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Identity;

namespace Freetalent.IdentityServer.Api.Configurations
{
    public class MyProfileService : IProfileService
    {
        private readonly UserManager<User> _userManager;

        public MyProfileService(UserManager<User> userManager)
        {
            _userManager = userManager;
        }

        public async Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            var user =_userManager.FindByIdAsync(context.Subject.GetSubjectId());
            var subjectClaims = context.Subject.Claims.ToList();

            var userClaims = await _userManager.GetClaimsAsync(await user);
            subjectClaims.AddRange(userClaims);
            // I can add another claims here, if I want.. 
            
            context.IssuedClaims.AddRange(subjectClaims);
        }

        public async Task IsActiveAsync(IsActiveContext context)
        {
            var sub = context.Subject.GetSubjectId();
            var user = await _userManager.FindByIdAsync(sub);

            context.IsActive = user != null;
        }
    }
}