﻿using System.Threading.Tasks;

namespace Freetalent.IdentityServer.Api.IdentityService
{
    public interface IIdentityService
    {
        Task<TokenDto> GetToken(TokenRequestDto request);
        Task<TokenDto> RefreshToken(RefreshTokenRequestDto request);
    }
}