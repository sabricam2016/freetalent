﻿namespace Freetalent.IdentityServer.Api.IdentityService
{
    public class RefreshTokenRequestDto
    {
        public string RefreshToken { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
    }
}