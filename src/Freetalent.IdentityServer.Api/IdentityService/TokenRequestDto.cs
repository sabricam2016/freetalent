﻿namespace Freetalent.IdentityServer.Api.IdentityService
{
    public class TokenRequestDto
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
    }
}