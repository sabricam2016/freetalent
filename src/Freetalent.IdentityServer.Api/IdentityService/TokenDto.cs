﻿namespace Freetalent.IdentityServer.Api.IdentityService
{
    public class TokenDto
    {
        public string AccessToken { get; set; }
        public int ExpiresIn { get; set; }
        public string RefreshToken { get; set; }
    }
}