﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using IdentityModel.Client;

namespace Freetalent.IdentityServer.Api.IdentityService
{
    public class IdentityService : IIdentityService
    {
        private readonly HttpClient _httpClient;
        public IdentityService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }
        
        public async Task<TokenDto> GetToken(TokenRequestDto request)
        {
            
                var discoResponse = await DiscoverDocumentAsync();
                var response = await _httpClient.RequestPasswordTokenAsync(new PasswordTokenRequest()
                {
                    Address = discoResponse.TokenEndpoint,
                    UserName = request.Username,
                    Password = request.Password,
                    ClientId = request.ClientId,
                    ClientSecret = request.ClientSecret
                });

                if (response.IsError)
                    throw new Exception(response.Error);
                
                return new TokenDto
                {
                    AccessToken = response.AccessToken,
                    ExpiresIn = response.ExpiresIn,
                    RefreshToken = response.RefreshToken
                };
        }

        public async Task<TokenDto> RefreshToken(RefreshTokenRequestDto request)
        {
            using (var httpClient = new HttpClient())
            {
                var response = await httpClient.RequestRefreshTokenAsync(new RefreshTokenRequest()
                {
                    Address = IdentityServerSettings.Authority,
                    ClientId = request.ClientId,
                    ClientSecret = request.ClientSecret,
                    RefreshToken = request.RefreshToken
                });

                return new TokenDto
                {
                    AccessToken = response.AccessToken,
                    ExpiresIn = response.ExpiresIn,
                    RefreshToken = response.RefreshToken
                };
            }
        }
        
        private static async Task<DiscoveryDocumentResponse> DiscoverDocumentAsync()
        {
            var docRequest = new DiscoveryDocumentRequest
            {
                Policy =
                {
                    RequireHttps = false,
                    ValidateIssuerName = false,
                    ValidateEndpoints = false,
                },
                Address = IdentityServerSettings.Authority
            };

            var client = new HttpClient();
            var discoResponse = await client.GetDiscoveryDocumentAsync(docRequest);

            if (discoResponse.IsError)
                throw new Exception(discoResponse.Error);

            return discoResponse;
        }
    }
}