﻿using System.Threading.Tasks;
using Freetalent.IdentityServer.Api.IdentityService;
using Microsoft.AspNetCore.Mvc;

namespace Freetalent.IdentityServer.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AccountController : ControllerBase
    {
        private readonly IIdentityService _identityService;

        public AccountController(IIdentityService identityService)
        {
            _identityService = identityService;
        }

        [HttpPost]
        public async Task<ActionResult<TokenDto>> GetToken(TokenRequestDto request)
        {
            return Ok(await _identityService.GetToken(request));
            
        }
        
        [HttpPost("refresh-token")]
        public async Task<ActionResult<TokenDto>> RefreshToken(RefreshTokenRequestDto request)
        {
            return Ok(await _identityService.RefreshToken(request));
        }
    }
}