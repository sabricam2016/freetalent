﻿using System.Threading.Tasks;
using Freetalent.Domain.Projects.Dtos;

namespace Freetalent.Domain.Projects
{
    public interface IProjectDomainService
    {
        Project Create(CreateProjectDto projectDto);
    }
}