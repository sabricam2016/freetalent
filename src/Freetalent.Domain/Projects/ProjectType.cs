﻿namespace Freetalent.Domain.Projects
{
    public enum ProjectType
    {
        Web,
        BackEnd,
        FrontEnd,
        EmbeddedSystems,
        Mobile,
    }
}