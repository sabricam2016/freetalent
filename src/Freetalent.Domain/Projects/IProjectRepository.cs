﻿using System;
using System.Threading.Tasks;
using Freetalent.Common.Repository;

namespace Freetalent.Domain.Projects
{
    public interface IProjectRepository : IRepository<Project,Guid>
    {
        Task Add(Project entity);
    }
}