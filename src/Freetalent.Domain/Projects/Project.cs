﻿using System;
using System.Collections.Generic;
using Freetalent.Common.Domain;

namespace Freetalent.Domain.Projects
{
    public class Project : AggregateRoot<Guid>
    {
        public string Title { get; private set; }

        public string Content { get; private set; }

        public ProjectType ProjectType { get; set; }

        
        public void SetTitle(string title)
        {
            // domain logic validation or other logics
            if (string.IsNullOrWhiteSpace(title))
                return; // throw exception

            Title = title;
        }

        public void SetContent(string content)
        {
            // domain logic validation or other logics
            if (string.IsNullOrWhiteSpace(content))
                return; // throw exception
            
            Content = content;
        }
    }
}