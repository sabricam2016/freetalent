﻿using Freetalent.Domain.Projects.Dtos;

namespace Freetalent.Domain.Projects
{
    public class ProjectDomainService : IProjectDomainService
    {
        private readonly IProjectRepository _projectRepository;

        public ProjectDomainService(IProjectRepository projectRepository)
        {
            _projectRepository = projectRepository;
        }

        public Project Create(CreateProjectDto projectDto)
        {
            var entity = new Project();
            entity.SetTitle(projectDto.Title);
            entity.SetContent(projectDto.Content);
            entity.ProjectType = projectDto.ProjectType;
            _projectRepository.Create(entity);
            
            return entity;
        }
    }
}