﻿namespace Freetalent.Domain.Projects.Dtos
{
    public class CreateProjectDto
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public ProjectType ProjectType { get; set; }
    }
}