﻿using System;
using System.Collections.Generic;
using System.Linq;
using Freetalent.Common.Domain;

namespace Freetalent.Domain.Accounts
{
    public class Account : AggregateRoot<Guid>
    {
        public string Username { get; private set; }
        public string Firstname { get; private set; }
        public string Lastname { get; private set; }
        public AccountType AccountType { get; set; }
        public List<Address> Addresses { get; set; } = new List<Address>();

        public Account()
        {
            Id = Guid.NewGuid();
        }

        public void SetUsername(string username)
        {
            if (string.IsNullOrWhiteSpace(username))
                throw new Exception("Username can not be empty");
            
            Username = username;
        }

        public void SetFirstname(string firstName)
        {
            if (string.IsNullOrWhiteSpace(firstName))
                throw new Exception("First name can not be empty");
            
            Firstname = firstName;
        }

        public void SetLastname(string lastName)
        {
            if (string.IsNullOrWhiteSpace(lastName))
                throw new Exception("Last name can not be empty");

            Lastname = lastName;
        }

        public void AddAddresses(List<Address> addresses)
        {
            if (addresses == null || !addresses.Any())
                throw new Exception("Address list can not be null");
            
            Addresses.AddRange(addresses);
        }
    }

    public class AccountConsts
    {
        public const int UsernameMaxLength = 256;
        public const int FirstnameMaxLength = 256;
        public const int LastnameMaxLength = 256;
    }
}