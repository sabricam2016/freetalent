﻿using System;
using Freetalent.Common.Repository;

namespace Freetalent.Domain.Accounts
{
    public interface IAccountRepository : IRepository<Account, Guid>
    {
        
    }
}