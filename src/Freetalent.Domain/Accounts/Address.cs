﻿using System;
using Freetalent.Common.Domain;

namespace Freetalent.Domain.Accounts
{
    public class Address : Entity<Guid>
    { 
        public string PhoneCode { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
    }

    public class AddressConsts
    {
        public const int MaxLengthPhoneCode = 5;
        public const int MaxLengthPhoneNumber = 11;
        public const int MaxLengthEmail = 30;
    }
}