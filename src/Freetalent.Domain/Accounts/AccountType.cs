﻿namespace Freetalent.Domain.Accounts
{
    public enum AccountType
    {
        Freelancer,
        ProjectOwner
    }
}