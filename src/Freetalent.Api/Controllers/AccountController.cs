﻿using System.Threading.Tasks;
using Freetalent.Application.Accounts.Commands;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Freetalent.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AccountController : ControllerBase
    {
        private readonly IMediator _mediator;

        public AccountController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost("register")]
        public async Task<IActionResult> RegisterAccount(RegisterAccountCommand command)
        {
            return Ok(await _mediator.Send(command));
        }
        
        [Authorize]
        [HttpGet]
        public  ActionResult Test()
        {
            return Ok("");
        }
    }
}