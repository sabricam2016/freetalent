﻿using System.Threading.Tasks;
using Freetalent.Application.Projects.Commands;
using Freetalent.Application.Projects.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Freetalent.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProjectController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly ILogger<ProjectController> _logger;

        public ProjectController(IMediator mediator, ILogger<ProjectController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult<ProjectDto>> Create(CreateProjectCommand command)
        {
            return Ok(await _mediator.Send(command));
        }
        
        [HttpGet]
        public ActionResult Get()
        {
            _logger.LogInformation("Project Controller Get end point was triggered!");
            return Ok("");
        }
    }
}