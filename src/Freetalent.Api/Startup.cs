using System;
using System.Reflection;
using Freetalent.Api.Configurations;
using Freetalent.Application.Accounts.Commands;
using Freetalent.Application.Projects.Commands;
using Freetalent.Common.UnitOfWork;
using Freetalent.Common.User;
using Freetalent.Domain.Accounts;
using Freetalent.Domain.Projects;
using Freetalent.Entityframework;
using Freetalent.Entityframework.Repositories;
using Freetalent.Master.Domain.Users;
using Freetalent.Master.Entityframework;
using MapsterMapper;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Freetalent.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            LocalUserContext.Instance.SetUser(new AsyncLocalUser()
            {
                TenantId = Guid.Empty
            });
            
            services.AddDbContext<AppDbContext>(options =>
                options.UseNpgsql(Configuration.GetConnectionString("Default")));

            services.AddMediatR(typeof(CreateProjectCommand).GetTypeInfo().Assembly);
           
            services.AddScoped<IMapper, Mapper>();
            services.AddScoped<IAccountRepository, AccountRepository>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IUserProvider, UserProvider>();
            services.AddScoped<ICurrentUser, CurrentUser>();
            services.AddScoped<IProjectRepository, ProjectRepository>();
            services.AddScoped<IProjectDomainService, ProjectDomainService>();

            services.AddDbContext<MasterDbContext>(options =>
                options.UseNpgsql(Configuration.GetConnectionString("Default")));

            services.AddIdentity<User, IdentityRole<Guid>>()
                .AddEntityFrameworkStores<MasterDbContext>()
                .AddDefaultTokenProviders();
            
            services.AddMySwagger();
            services.AddMyAuthentication(Configuration);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
            }
        
            app.UseMySwagger();
            app.UseHttpsRedirection();

            new MapsterProfile().Configure();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}