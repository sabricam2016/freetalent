using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.Elasticsearch;

namespace Freetalent.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args)
                .UseSerilog((hostBuilder, loggerConfiguration) => 
                {
                    loggerConfiguration
                        .MinimumLevel.Debug()
                        .Enrich.FromLogContext()
                        .Enrich.WithProperty("Environment", hostBuilder.HostingEnvironment.EnvironmentName)
                        .MinimumLevel.Override("Microsoft", hostBuilder.HostingEnvironment.IsDevelopment() 
                            ? LogEventLevel.Debug : LogEventLevel.Warning)
                        .WriteTo.Console()
                        .WriteTo.Elasticsearch(new ElasticsearchSinkOptions(new Uri("http://localhost:9200"))
                        {
                            AutoRegisterTemplate = true,
                            AutoRegisterTemplateVersion = AutoRegisterTemplateVersion.ESv7,
                            IndexFormat = "ft-api-log",
                            MinimumLogEventLevel = LogEventLevel.Debug
                        });
                    
                    if (hostBuilder.HostingEnvironment.IsDevelopment())
                    {
                        loggerConfiguration.MinimumLevel.Override("FreeTalent", LogEventLevel.Debug);
                        loggerConfiguration.MinimumLevel.Override("FreeTalent is starting", LogEventLevel.Debug);

                    }
                })
                .Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });
        
    }
}