﻿using Freetalent.Application.Accounts.Queries;
using Freetalent.Domain.Accounts;
using Mapster;

namespace Freetalent.Api.Configurations
{
    public class MapsterProfile
    {
        public void Configure()
        {
            TypeAdapterConfig.GlobalSettings.Default.NameMatchingStrategy(NameMatchingStrategy.Flexible);

            TypeAdapterConfig<Account, AccountDto>
                .NewConfig();
        }
    }
}