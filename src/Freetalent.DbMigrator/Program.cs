﻿using System;
using System.IO;
using Freetalent.Common.User;
using Freetalent.Entityframework;
using Freetalent.Master.Entityframework;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace Freetalent.DbMigrator
{
    class Program
    {
        private static IConfigurationRoot configuration;

        static void Main(string[] args)
        {
            var serviceCollection = new ServiceCollection();
            Configuration(serviceCollection);
            IServiceProvider serviceProvider = serviceCollection.BuildServiceProvider();
            
            MasterDbContextMigrate(serviceProvider);
            AppDbContextMigrate(serviceProvider);
        }

        private static void MasterDbContextMigrate(IServiceProvider serviceProvider)
        {
            try
            {
                var dbContext = serviceProvider.GetRequiredService<MasterDbContext>();
                dbContext.Database.Migrate();
                Console.WriteLine($"Migration succeed for MasterDbContext");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Could not migrate for MasterDbContext : {ex.Message}");
            }
        }

        private static void AppDbContextMigrate(IServiceProvider serviceProvider)
        {
            try
            {
                LocalUserContext.Instance.SetUser(new AsyncLocalUser
                {
                    TenantId = Guid.Empty
                });
                
                var dbContext = serviceProvider.GetRequiredService<AppDbContext>();
                dbContext.Database.Migrate();
                Console.WriteLine($"Migration succeed for AppDbContext");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Could not migrate for AppDbContext : {ex.Message}");
            }
        }

        private static void Configuration(IServiceCollection serviceCollection)
        {
            configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetParent(AppContext.BaseDirectory)?.FullName)
                .AddJsonFile("appsettings.json", false)
                .Build();

            serviceCollection.AddSingleton<IConfigurationRoot>(configuration);
            serviceCollection.AddScoped<IUserProvider, UserProvider>();
            serviceCollection.AddScoped<ICurrentUser, CurrentUser>();
            serviceCollection.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            serviceCollection.AddDbContext<MasterDbContext>(options =>
                options.UseNpgsql(configuration.GetConnectionString("Default"),
                    options => options.MigrationsHistoryTable("_EFMigrationsHistory", "master")
                ));

            serviceCollection.AddDbContext<AppDbContext>(options =>
                options.UseNpgsql(configuration.GetConnectionString("Default"),
                    options=> options.MigrationsHistoryTable("_EFMigrationsHistory","Freetalent")));
        }
    }
}