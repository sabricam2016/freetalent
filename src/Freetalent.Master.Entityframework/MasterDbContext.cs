﻿using System;
using Freetalent.Master.Domain.Users;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Freetalent.Master.Entityframework
{
    public class MasterDbContext : IdentityDbContext<User, IdentityRole<Guid>, Guid>
    {
        public MasterDbContext(DbContextOptions<MasterDbContext> options) : base(options)
        {
            
        }
        
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.HasDefaultSchema("master");
            
            builder.Entity<User>(b =>
            {
                b.ToTable("users");
            });
        }
    }
}