﻿using System.Collections.Generic;

namespace Freetalent.Application.Accounts.Queries
{
    public class AccountDto
    {
        public string Username { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public List<AddressDto> Addresses { get; set; } = new List<AddressDto>();

        public class AddressDto
        {
            public string Email { get; set; }
            public string PhoneCode { get; set; }
            public string PhoneNumber { get; set; }
        }
    }
}