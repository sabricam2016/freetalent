﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Freetalent.Application.Accounts.Queries;
using Freetalent.Common.UnitOfWork;
using Freetalent.Common.User;
using Freetalent.Domain.Accounts;
using Freetalent.Master.Domain.Users;
using MapsterMapper;
using MediatR;
using Microsoft.AspNetCore.Identity;

namespace Freetalent.Application.Accounts.Commands
{
    public class RegisterAccountCommandHandler : IRequestHandler<RegisterAccountCommand, AccountDto>
    {
        private readonly IAccountRepository _repository;
        private readonly UserManager<User> _userManager;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public RegisterAccountCommandHandler(IAccountRepository repository,
            UserManager<User> userManager, IMapper mapper,
            IUnitOfWork unitOfWork)
        {
            _repository = repository;
            _userManager = userManager;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public  async Task<AccountDto> Handle(RegisterAccountCommand request, CancellationToken cancellationToken)
        {
            return await _unitOfWork.RunInTransactionScope<AccountDto>(async () =>
            {
                var account = CreateAccount(request);
                _repository.Create(account);
                var result = await _userManager.CreateAsync(CreateIdentityUser(request), request.Password);
                
                LocalUserContext.Instance.SetUser(
                    new AsyncLocalUser
                    {
                        TenantId = account.Id
                    });

                
                if (!result.Succeeded)
                    throw new ValidationException(ListErrorAsString(result.Errors));
                
                await _unitOfWork.SaveChangesAsync();
                return _mapper.Map<AccountDto>(account);
            });
        }

        private static User CreateIdentityUser(RegisterAccountCommand request)
        {
            return new()
            {
                UserName = request.Username,
                Email = request.Addresses.FirstOrDefault()?.Email,
                PhoneNumber = request.Addresses.FirstOrDefault()?.PhoneNumber,
            };
        }

        private static Account CreateAccount(RegisterAccountCommand request)
        {
            var account = new Account();

            account.AccountType = AccountType.Freelancer;
            account.SetFirstname(request.Firstname);
            account.SetLastname(request.Lastname);
            account.SetUsername(request.Username);
            account.AddAddresses(request.Addresses.Select(x =>
                new Address
                {
                    PhoneCode = x.PhoneCode,
                    Email = x.Email,
                    PhoneNumber = x.PhoneNumber
                }).ToList());
            return account;
        }

        private string ListErrorAsString(IEnumerable<IdentityError> errors)
        {
            var errorStatement = new StringBuilder();
            foreach (var error in errors)
            {
                errorStatement.Append(error.Code + " " + error.Description + "\n");
            }

            return errorStatement.ToString();
        }
    }
}