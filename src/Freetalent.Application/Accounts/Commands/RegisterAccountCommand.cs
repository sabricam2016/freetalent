﻿using System.Collections.Generic;
using Freetalent.Application.Accounts.Queries;
using Freetalent.Domain.Accounts;
using MediatR;

namespace Freetalent.Application.Accounts.Commands
{
    public class RegisterAccountCommand : IRequest<AccountDto>
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public AccountType AccountType { get; set; }
        public List<AddressDto> Addresses { get; set; } = new List<AddressDto>();

        public class AddressDto
        {
            public string Email { get; set; }
            public string PhoneCode { get; set; }
            public string PhoneNumber { get; set; }
        }
    }
}