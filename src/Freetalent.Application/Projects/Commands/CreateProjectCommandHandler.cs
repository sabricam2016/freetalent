﻿using System.Threading;
using System.Threading.Tasks;
using Freetalent.Application.Projects.Queries;
using Freetalent.Common.UnitOfWork;
using Freetalent.Domain.Projects;
using Freetalent.Domain.Projects.Dtos;
using MapsterMapper;
using MediatR;

namespace Freetalent.Application.Projects.Commands
{
    public class CreateProjectCommandHandler : IRequestHandler<CreateProjectCommand, ProjectDto>
    {
        private readonly IProjectDomainService _projectDomainService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CreateProjectCommandHandler(IProjectDomainService projectDomainService, IUnitOfWork unitOfWork,
            IMapper mapper)
        {
            _projectDomainService = projectDomainService;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<ProjectDto> Handle(CreateProjectCommand request, CancellationToken cancellationToken)
        {
            var createProjectDto = _mapper.Map<CreateProjectDto>(request);
            var result = _projectDomainService.Create(createProjectDto);

            await _unitOfWork.SaveChangesAsync();
            return _mapper.Map<ProjectDto>(result);
        }
    }
}