﻿using Freetalent.Application.Projects.Queries;
using Freetalent.Domain.Projects;
using MediatR;

namespace Freetalent.Application.Projects.Commands
{
    public class CreateProjectCommand : IRequest<ProjectDto>
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public ProjectType ProjectType { get; set; }
    }
}