﻿using System;
using Freetalent.Domain.Projects;

namespace Freetalent.Application.Projects.Queries
{
    public class ProjectDto
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public ProjectType ProjectType { get; set; }
    }
}