﻿using System;

namespace Freetalent.Common.Domain
{
    public interface IEntity<TId> where TId : IEquatable<TId>
    {
        TId Id { get; set; }
    }
}