﻿using System;

namespace Freetalent.Common.Domain
{
    public class Entity<TId> : IEntity<TId> where TId : IEquatable<TId>
    {
        public TId Id { get; set; }
    }
}