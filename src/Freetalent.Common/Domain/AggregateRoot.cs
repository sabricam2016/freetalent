﻿using System;

namespace Freetalent.Common.Domain
{
    public class AggregateRoot<TId> : IEntity<TId>, IAggregateRoot where TId : IEquatable<TId>
    {
        public TId Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid? UpdatedBy { get; set; }
    }
}