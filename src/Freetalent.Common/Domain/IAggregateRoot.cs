﻿using System;

namespace Freetalent.Common.Domain
{
    public interface IAggregateRoot
    {
        DateTime CreatedDate { get; set; }
        DateTime? UpdatedDate { get; set; }
        Guid CreatedBy { get; set; }
        Guid? UpdatedBy { get; set; }
    }
}