﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Http;

namespace Freetalent.Common.User
{
    public class CurrentUser : ICurrentUser
    {
        private readonly HttpContext _httpContext;

        public CurrentUser(IHttpContextAccessor accessor)
        {
            _httpContext = accessor.HttpContext;
        }
        
        public  Guid TenantId => 
            Guid.Parse(_httpContext.User.Claims.FirstOrDefault(x => x.Type == "TenantId")?.Value ?? string.Empty);

        public bool IsAuthenticated()
        {
            return _httpContext.User.Identity != null 
                   && _httpContext.User.Identity.IsAuthenticated;
        }
    }
}