﻿using System.Threading.Tasks;

namespace Freetalent.Common.User
{
    public interface IUserProvider
    {
        ICurrentUser GetCurrentUser();
    }
}