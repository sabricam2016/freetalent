﻿namespace Freetalent.Common.User
{
    public class UserProvider : IUserProvider
    {
        private readonly ICurrentUser _user;

        public UserProvider(ICurrentUser user)
        {
            _user = user;
        }

        public ICurrentUser GetCurrentUser()
        {
            if (LocalUserContext.Instance.GetUser() != null)
                return LocalUserContext.Instance.GetUser();

            if (_user.IsAuthenticated() && _user != null)
                return _user;

            return null;
        }
    }
}