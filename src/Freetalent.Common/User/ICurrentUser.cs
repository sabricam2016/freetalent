﻿using System;

namespace Freetalent.Common.User
{
    public interface ICurrentUser
    {
        Guid TenantId { get; }

        bool IsAuthenticated();
    }
}