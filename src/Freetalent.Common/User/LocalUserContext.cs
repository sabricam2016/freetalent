﻿using System;
using System.Threading;

namespace Freetalent.Common.User
{
    public class LocalUserContext
    {
        public static readonly LocalUserContext Instance = new LocalUserContext();
        private static AsyncLocal<ICurrentUser> User;
        
        private LocalUserContext()
        {
            
        }

        public void SetUser(ICurrentUser user)
        {
            if (User == null)
                User = new AsyncLocal<ICurrentUser>();
            
            User.Value = user;
        }
        
        public ICurrentUser GetUser()
        {
            return User?.Value;
        }
    }
}