﻿using System;
using System.Threading.Tasks;

namespace Freetalent.Common.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        Task SaveChangesAsync();

        Task RunInDbTransaction(Func<Task> action);

        Task RunInTransactionScope(Func<Task> action);
        
        Task<T> RunInTransactionScope<T>(Func<Task<T>> action);

    }
}