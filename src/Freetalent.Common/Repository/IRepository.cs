﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Freetalent.Common.Domain;

namespace Freetalent.Common.Repository
{
    public interface IRepository<TEntity, TId> where TEntity : AggregateRoot<TId>,new() where  TId : IEquatable<TId>
    {
        void Create(TEntity entity);
        
        void Update(TEntity entity);

        Task<TEntity> GetById(TId id);

        Task<IList<TEntity>> GetList();
        
        Task<TEntity> GetById(TId id, params Expression<Func<TEntity, object>>[] includes);
        
        Task<IList<TEntity>> GetList(params Expression<Func<TEntity, object>>[] includes);

        IQueryable<TEntity> GetQueryAsNoTracking();
        
        IQueryable<TEntity> GetQuery();
    }
}