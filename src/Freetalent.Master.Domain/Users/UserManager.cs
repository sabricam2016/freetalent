﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace Freetalent.Master.Domain.Users
{
    public class UserManager : IUserManager
    {
        private readonly UserManager<User> _userManager;

        public UserManager(UserManager<User> userManager)
        {
            _userManager = userManager;
        }

        public async Task Create(User entity, string password, List<Claim> claims = null)
        {
            var result = await _userManager.CreateAsync(entity, password);

            if (result.Succeeded)
            {   
                if(claims!=null && claims.Any())
                    claims.ForEach(async cl => await  _userManager.AddClaimAsync(entity, cl));
                else
                    await _userManager.AddClaimAsync(entity, new Claim("tenantCode", "FreeTalent"));
            }
            else
                throw new Exception("User could not create!");
        }

        public Task Update(User entity)
        {
            throw new System.NotImplementedException();
        }
    }
}