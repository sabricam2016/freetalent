﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Freetalent.Master.Domain.Users
{
    public interface IUserManager
    {
        Task Create(User entity, string password, List<Claim> claims);
        Task Update(User entity);
        
    }
}