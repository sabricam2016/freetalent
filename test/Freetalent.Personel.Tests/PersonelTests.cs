﻿using Freetalent.SampleTwo;
using Xunit;

namespace Freetalent.Personel.Tests
{
    public class PersonelTests
    {
        [Fact]
        public void should_combine_second()
        {
            var sinif = new SampleTwoClass();

            var result = sinif.SecondCombineName("ali", "veli");
            Assert.Equal("ali veli",result);
        }  
        
        [Fact]
        public void two()
        {
            var sinif = new SampleTwoClass();

            var result = sinif.ThirdCombineName("ali", "veli");
            Assert.Equal("ali veli",result);
        }  
        
        [Fact]
        public void third()
        {
            var sinif = new SampleTwoClass();

            var result = sinif.FourthCombineName("ali", "veli");
            Assert.Equal("ali veli",result);
        }  
        
        
        [Fact]
        public void fourth()
        {
            var sinif = new SampleTwoClass();

            var result = sinif.FifthCombineName("ali", "veli");
            Assert.Equal("ali veli",result);
        }  
    }
}