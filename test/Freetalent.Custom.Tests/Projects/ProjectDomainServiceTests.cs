﻿using Freetalent.SampleProject;
using Freetalent.SampleTwo;
using Xunit;

namespace Freetalent.Domain.Tests.Projects
{
    public class ProjectDomainServiceTests
    {  /*
        private readonly Mock<IProjectRepository> _projectRepository;
        private readonly ProjectDomainService _projectDomainService;
        
        public ProjectDomainServiceTests()
        {
            _projectRepository = new Mock<IProjectRepository>();
            _projectDomainService = new ProjectDomainService(_projectRepository.Object);
        }
    /*
        [Fact]
        public void should_create_project()
        {
            var createProjectDto = new CreateProjectDto
            {
                Content = "Project_Content",
                Title = "Project_Title",
                ProjectType = ProjectType.BackEnd
            };

            var result = _projectDomainService.Create(createProjectDto);
            
            // projectDomainService includes some domain logic and validation
            // actually we test that these logics and validations here
            
            Assert.Equal(createProjectDto.Content, result.Content);
            Assert.Equal(createProjectDto.Title, result.Title);
            Assert.Equal(createProjectDto.ProjectType, result.ProjectType);
        }  */
        
    
        [Fact]
        public void should_sum_first()
        {
            var sinif = new Class1();

            var result = sinif.Sum(4, 6);
            Assert.Equal(10,result);
        } 
         [Fact]
                public void should_combine_first()
                {
                    var sinif = new SampleTwoClass();
        
                    var result = sinif.CombineName("ali", "veli");
                    Assert.Equal("ali veli",result);
                }  
       
    }
}